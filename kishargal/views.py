from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import my_schedule
from .models import Schedule
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView

# Create your views here.

response = {}
def awal(request):
    return render(request, 'Awal.html', {})

def profile(request):
    return render(request, 'Story3.html', {})

def arsip(request):
    return render(request, 'Arsip.html', {})

def schedule(request):
    response['my_schedule'] = my_schedule
    return render(request, 'schedule.html', response)


def add_schedule(request):
    form = my_schedule(request.POST or None)
    
    if (request.method == 'POST'):
        response['activity'] = request.POST.get('activity', '')
        response['tanggal'] = request.POST.get('tanggal', '')
        response['location'] = request.POST.get('location', '')
        response['category'] = request.POST.get('category', '')
        response['details'] = request.POST.get('details', '')
        
        schedule = Schedule(activity=response['activity'], tanggal=response['tanggal'],
                          category=response['category'], location=response['location'], details=response['details'])
        schedule.save()
        response['data'] = Schedule.objects.all().values()
        return render(request, 'response.html', response)
    else:        
        return HttpResponseRedirect('')

def saved_schedule(request):
    response['data'] = Schedule.objects.all().values()
    return render(request, 'response.html', response)

def delete_schedule(request):
    response['data'] = Schedule.objects.all().delete()
    return render(request, 'response.html', response)
from django.db import models

# Create your models here.
    
class Schedule(models.Model):

    
    ACADEMY ="Akademis"
    ORGANIZATION = "Organisasi"
    FAMILY = "Keluarga"
    HAVE_FUN = "Hiburan"

    CATEG_CHOICES = (
        (ACADEMY, "Akademis"),
        (ORGANIZATION, "Organisasi"),
        (FAMILY, "Keluarga"),
        (HAVE_FUN, "Hiburan"),
    )

    activity = models.CharField(max_length=200)
    tanggal = models.DateTimeField(blank=False)
    location = models.CharField(max_length=200)
    category = models.CharField(max_length = 200, choices = CATEG_CHOICES)
    details = models.TextField()
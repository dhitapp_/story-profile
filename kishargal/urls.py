from django.urls import path
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#url for app
urlpatterns = [
    path('', awal, name='awal'),
    path('about/', profile, name='profile'),
    path('arsip/', arsip, name='arsip'),
    path('schedule/', schedule, name='schedule'),
    path('response/', add_schedule, name = "add_schedule"),
    path('my_schedule/', saved_schedule, name = "response"),
    path('delete/', delete_schedule, name="delete_schedule")
]

# Generated by Django 2.1.1 on 2018-10-03 20:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kishargal', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='activities',
            field=models.CharField(max_length=35),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='location',
            field=models.CharField(max_length=75),
        ),
    ]

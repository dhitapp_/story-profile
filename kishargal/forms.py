from django import forms

class my_schedule(forms.Form):

    ACADEMY ="Akademis"
    ORGANIZATION = "Organisasi"
    FAMILY = "Keluarga"
    HAVE_FUN = "Hiburan"

    CATEG_CHOICES = (
        (ACADEMY, "Akademis"),
        (ORGANIZATION, "Organisasi"),
        (FAMILY, "Keluarga"),
        (HAVE_FUN, "Hiburan"),
    )

    attrs = {
        'class': 'form-control'
    }

    activity = forms.CharField(label='Name of Activity', max_length=200, widget=forms.TextInput(attrs=attrs), required = True)
    tanggal = forms.DateTimeField(label='Date', widget=forms.DateTimeInput(attrs={'type':'datetime-local','class': 'form-control'}), input_formats=('%d/%m/%Y %H:%M'),required = True)
    location = forms.CharField(label='Location', max_length=200, widget=forms.TextInput(attrs=attrs), required = True)
    category = forms.ChoiceField(label='Category', choices = CATEG_CHOICES, widget=forms.Select(attrs=attrs), required = True)
    details = forms.CharField(label='Detail', max_length=200, widget=forms.Textarea(attrs=attrs)) 